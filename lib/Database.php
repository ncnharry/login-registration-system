<?php


class Database
{


	public $dbh;


	public function __construct (){
		try {
			$this->dbh = new PDO('mysql:host=localhost;dbname=db_lr', 'root', '4833');
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch (PDOException $e){
			echo "Failed to connect database".$e->getMessage();
		}


	} // End of Construct method

}
