<?php
    include 'inc/header.php';
    include 'lib/User.php';
    Session::checkSession();


    $user = new User();
    $allData = $user->index();

 $loginMsg = Session::get("loginmsg");

 if (isset($loginMsg)){
     echo $loginMsg;
 }

 Session::set('loginmsg', NULL);
 
 ?>



  <!--  Breadcrumb -->
  <div class="container">
      <div class="breadcrumb">
          <a href="index.php" class="active">Home</a>
      </div>
  </div>
  <!--  // Breadcrumb -->

<!--  User list -->

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">User list <span class="pull-right">Welcome !
                <?php

                $name = Session::get('username');
                if (isset($name)){
                    echo "<b>".$name."</b>" ;
                }

                ?>
                </span></h3>
        </div >
        <div class="panel-body">
            <table class="table table-striped">
            <thead >
                <th width="20%">Serial</th>
                <th width="20%">Name</th>
                <th width="20%">Username</th>
                <th width="20%">Email</th>
                <th width="20%">Action</th>
            </thead>
            <tbody>
            <?php
               $serial = 1;
                foreach($allData as $data ){
                    echo "
                    <tr> 
                    <td>$serial</td>
                    <td>$data->name</td>
                    <td>$data->username</td>
                    <td>$data->email</td>
                    <td>  
                    <a href='profile.php?id=$data->id'  class='btn btn-info' role='button'><span class='glyphicon glyphicon-eye-open'> View</a>

                    <a href='profile.php?id=$data->id'  class='btn btn-primary' role='button' name='edit' ><span class='glyphicon glyphicon-eye-open'> Edit</a>      
                
                    </td>
               
                            
                   </tr>
                    
                    ";
                    $serial++;
                }
               


                ?>  

            </tbody>
            </table>
        </div>
    </div>
</div>  <!-- // user list ended here -->



<?php require_once 'inc/footer.php';?>
















