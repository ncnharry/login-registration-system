<?php 

$filepath = realpath(dirname(__FILE__));
// echo $filepath;

include_once $filepath.'/../lib/Session.php';
Session::init();

if (isset($_GET['action']) && $_GET['action'] == 'logout') {
	Session::destroy();
}
?>


<!doctype html>
<html lang="en">
<head>
	<title>Login Registration System</title>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="inc/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="inc/main.css">

</head>
<body>

<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">Login Registration</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"><a href="index.php">Users <span class="sr-only">(current)</span></a></li>
				<li><a href="#">Link</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				
				<?php 
					$id = Session::get('id');
					$userlogin = Session::get('login');
					if($userlogin == true){
				?>
				<li><a href="index.php">Home</a></li>
				<li><a href="profile.php">Profile</a></li>
				<li><a href="?action=logout">Logout</a></li>
				
				<?php } else {?>

					<li><a href="login.php">Login</a></li>
					<li><a href="register.php">Register</a></li>

				<?php } ?>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container -->
</nav>

<br>