<?php
require_once 'inc/header.php';
require_once 'lib/User.php';

$user = new User();
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])){
    $userReg = $user->register($_POST);


}
?>


	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title text-center">Register an User</h3>
					</div >
					<div class="panel-body">
                       
						<form action="register.php" method="POST">
							 <?php if (isset($userReg)){ echo $userReg;}?>
							<div class="form-group">
								<label for="">Your Name:</label>
								<input type="text" class="form-control" name="name">
							</div>
							<div class="form-group">
								<label for="">Username:</label>
								<input type="text" class="form-control" name="username">
							</div>
							<div class="form-group">
								<label for="">Email:</label>
								<input type="text" class="form-control" name="email">
							</div>
							<div class="form-group">
								<label for="">Password:</label>
								<input type="text" class="form-control" name="password">
							</div>



							<button class="btn btn-success pull-right" type="submit" name="register"> Register </button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>  <!-- // login container ended here -->


<?php  require_once 'inc/footer.php'; ?>
